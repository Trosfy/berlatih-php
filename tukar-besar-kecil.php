<?php
function tukar_besar_kecil($string){
//kode di sini
    for($i=0;$i<strlen($string);$i++)
    {
        $num = ord($string[$i]);
        if($num>=ord("a") && $num<=ord("z"))
        {
            $num = $num-ord("a")+ord("A");
            $string[$i] = chr($num);
        }else if($num>=ord("A") && $num<=ord("Z"))
        {
            $num = $num-ord("A")+ord("a");
            $string[$i] = chr($num);
        }else continue;
    }
    return $string;
}

// TEST CASES
echo (tukar_besar_kecil('Hello World'))."<br>"; // "hELLO wORLD"
echo (tukar_besar_kecil('I aM aLAY'))."<br>"; // "i Am Alay"
echo (tukar_besar_kecil('My Name is Bond!!'))."<br>"; // "mY nAME IS bOND!!"
echo (tukar_besar_kecil('IT sHOULD bE me'))."<br>"; // "it Should Be ME"
echo (tukar_besar_kecil('001-A-3-5TrdYW'))."<br>"; // "001-a-3-5tRDyw"

?>