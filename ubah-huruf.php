<?php
function ubah_huruf($string){
//kode di sini
    // echo "\$string adalah ".$string;
    for($i=0;$i<strlen($string);$i++)
    {
        $num = ord($string[$i]);
        if($num>=ord("a") && $num<=ord("z"))
        {
            $num-=ord("a");
            $num+=1;
            $num%=26;
            $num+=ord("a");
            $string[$i] = chr($num);
        }else if($num>=ord("A") && $num<=ord("Z"))
        {
            $num-=ord("A");
            $num+=1;
            $num%=26;
            $num+=ord("a");
            $string[$i] = chr($num);
        }else continue;
    }
    return $string;
}

// TEST CASES
echo (ubah_huruf("wow"))."<br>"; // xpx
echo (ubah_huruf('developer'))."<br>"; // efwfmpqfs
echo (ubah_huruf('laravel'))."<br>"; // mbsbwfm
echo (ubah_huruf('keren'))."<br>"; // lfsfo
echo (ubah_huruf('semangat'))."<br>"; // tfnbohbu

?>